package q3;

import java.util.Random;
import java.util.Scanner;

public class Game {

	public static void main(String[] args) {
		Game game=new Game ();
		System.out.println("-----------------------------DIE ROLL GAME!-----------------------------");
		System.out.println("Please choose your small or large:");
		System.out.println("small or large");
		Scanner reader = new Scanner(System.in);
		String playerChoice = reader.next();
		
		System.out.println(game.playRound(600,playerChoice));
	}
	
	private int money;
	private final String[] smallOrLarge = {"small","large"};
	private Random rand;
	
	public Game() {
		money=500;
	}
	
	public int getMoney() {
		return this.money;
	}
	
	public String playRound(int betMoney,String betChoice) {
		if(isValidInput(betMoney)) {
		rand=new Random();
		int compChoice = rand.nextInt(2);
		System.out.println(compChoice);
		String dieRollResult = this.smallOrLarge[compChoice];
		System.out.println(dieRollResult);
		if(dieRollResult.equals(betChoice)) {
			this.money+=betMoney;
			return ("The die show a "+dieRollResult+". You WON! You have in total "+ money);
		} else {
			this.money-=betMoney;
			return ("The die show a "+dieRollResult+". You LOST! You have in total "+ money);
		}
		}
		else {
			if(betMoney<=0) {
				return("Please input a bet amount larger than 0 ");
			}
			else {
				return ("Please input a bet amount smaller than the money left.");
			
			}
		}
	}
	
	/*If the user has placed an invalid bet (e.g. they don�t have enough money, they didn�t enter a number, etc)
	 *  then there should be no change. A message should be displayed to the user indicating the issue.*/
	
	public boolean isValidInput(int betMoney) {
		System.out.println(betMoney);
		System.out.println("get"+ getMoney());
		if(betMoney > getMoney() || betMoney<=0) {
			return false;
		}
		else {
			return true;
		}
	}
}
