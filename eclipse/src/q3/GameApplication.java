//Cathy Tham
package q3;

import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;

public class GameApplication extends Application {	
	
	public void start(Stage stage) {
		Group root = new Group(); 
		
		GameContext gameContext=new GameContext();
		
		//Vbox added to the root
		root.getChildren().add(gameContext);
		
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.WHITE);
		
		//associate scene to stage and show
		stage.setTitle("Die Roll Game"); 
		stage.setScene(scene); 	
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}    

