//Cathy Tham
package q3;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.text.Text;


public class GameChoice implements EventHandler<ActionEvent>{
	private TextField betMoney;
	private Text message;
	private String playerChoice;
	private Game game;
	
	//constructor
	public GameChoice(TextField betMoney, Text message, String playerChoice, Game game) {
		this.betMoney=betMoney;
		this.message = message;
		this.playerChoice = playerChoice;
		this.game = game;
	}
	
	@Override
	public void handle(ActionEvent e) {
		if(isValidInput(this.betMoney.getText())) {
			int betMoney1= Integer.valueOf(this.betMoney.getText());
			
			String resultMessage = game.playRound(betMoney1,playerChoice);
			message.setText(resultMessage);	
		}
		else {
			message.setText("Please input a valid bet money (larger than 0 or smaller than your money left).");
		}
	}
	
	public boolean isValidInput(String betMoney) {
		try {
			int betAmount = Integer.valueOf(this.betMoney.getText());
			return true;
		}
		catch(NumberFormatException e) {
			return false;
		}
	}
}

