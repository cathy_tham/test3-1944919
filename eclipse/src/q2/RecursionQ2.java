/*Note: For this question, your main method will not be graded. However, you should test your program somehow 
 * (e.g. write a main method or a JUnit test) in order to know if it works correctly or not.

Inside of a class Recursion, and using recursion, write a method recursiveCount() that accepts as input an 
int[] numbers and an int n. The method should return the number of ints in numbers that:

� Are smaller than 10

� Are located within numbers at an even index (e.g. 0,2,4,6, etc) that is greater than or equal to n.

*/
package q2;

public class RecursionQ2 {
	public static void main(String[] args) {
		int[] arr = {5,10,2,6,3,20,30};
		System.out.println(recursiveCount(arr,40));
	} 
	
	public static int recursiveCount(int[] numbers, int n) {
		int index=0;
		return recursiveCount(numbers,n,index);
	}
	
	private static int recursiveCount(int[] numbers, int n, int index) {
		//if the index is bigger or equal to the arr length
		if (index >= numbers.length) {
			return 0;
		}
		
		int count = recursiveCount(numbers,n,index+1);
		//smaller than 10
		if(numbers[index] < 10) {
			return count+1;
			
		}
		//located at even index and greater or equal to n
		else if (index%2==0 && numbers[index]>=n){
			return count+1;
		} 
		else {
			return count;
		}
	}
}

