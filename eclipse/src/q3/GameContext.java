package q3;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.text.Text;


public class GameContext extends VBox {
	private Game game=new Game();
	
	public GameContext() {
		//each button added to HBOx buttons 
		HBox buttons = new HBox();
		Button smallButton= new Button("Small");
		Button largeButton= new Button("Large");
		buttons.getChildren().addAll(smallButton,largeButton);
		
		TextField amountMoney = new TextField("Please input amount of money!");
		//set width of message text field
		amountMoney.setPrefWidth(200);
		
		Text message= new Text();
		
		//add event handler to buttons
		GameChoice small =new GameChoice(amountMoney,message,"small",game);
		smallButton.setOnAction(small);
		GameChoice large=new GameChoice(amountMoney,message,"large",game);
		largeButton.setOnAction(large);
				
		//TextFields and buttons added to Vbox
		VBox overall = new VBox();
		//add multiple nodes to Vbox
		overall.getChildren().addAll(buttons,amountMoney,message);
		
		this.getChildren().add(overall);
	}
	
	

}
